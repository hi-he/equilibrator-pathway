import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from equilibrator_api import Q_, ComponentContribution
from sbtab import SBtab
from tqdm import tqdm

from equilibrator_pathway import ECMmodel, Pathway


comp_contrib = ComponentContribution.initialize_custom_version(
    quilt_version_cc="0.2.24", quilt_version_cache="0.2.9"
)

#%% example for MDF analysis

pp = Pathway.from_sbtab("ecoli_noor_2016_mdf.tsv", comp_contrib=comp_contrib)

mdf_result = pp.calc_mdf()

fig1 = mdf_result.compound_plot

fig2 = mdf_result.reaction_plot

#%% example for ECM analysis

model = ECMmodel.from_sbtab(
    "ecoli_noor_2016_ecm.tsv", comp_contrib=comp_contrib
)
model.AddValidationData("ecoli_noor_2016_reference.tsv")

print("Solving MDF problem")
lnC_MDF = model.MDF()
print("Solving ECM problem")
np.random.seed(1982)
lnC_ECM = model.ECM(n_iter=10)
res_sbtab = model.to_sbtab(lnC_ECM)

fig1 = plt.figure(figsize=(14, 5))
ax_MDF = fig1.add_subplot(1, 2, 1)
ax_MDF.set_title("MDF solution")
model.PlotEnzymeDemandBreakdown(lnC_MDF, ax_MDF, plot_measured=True)
ax_MDF.legend().set_visible(False)

ax_ECM = fig1.add_subplot(1, 2, 2, sharey=ax_MDF)
ax_ECM.set_title("ECM solution")
model.PlotEnzymeDemandBreakdown(lnC_ECM, ax_ECM, plot_measured=True)
ax_ECM.legend(loc="center left", bbox_to_anchor=(1, 0.5))

fig = plt.figure(figsize=(16, 5))
ax = fig.add_subplot(1, 3, 1, xscale="log", yscale="log")
ax.set_title("Metabolite Concentrations")
model.ValidateMetaboliteConcentrations(lnC_ECM, ax)

ax = fig.add_subplot(1, 3, 2, xscale="log", yscale="log")
ax.set_title("Enzyme Concentrations")
model.ValidateEnzymeConcentrations(lnC_ECM, ax)

ax = fig.add_subplot(1, 3, 3)
vols, labels, colors = model._GetVolumeDataForPlotting(lnC_ECM)
ax.pie(vols, labels=labels, colors=colors)
ax.set_title("total weight = %.2g [g/L]" % sum(vols))

output_fname = "ecm_results.tsv"
print(f"Writing the ECM results to file: {output_fname}")
res_sbtab.write(output_fname)
