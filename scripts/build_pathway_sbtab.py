from equilibrator_api import ComponentContribution
from equilibrator_pathway import Pathway, ECMmodel
import argparse

parser = argparse.ArgumentParser(
    description=
    "Script for building a pathway configuration SBtab (for ECM or MDF) "
    "from a network SBtab."
)
parser.add_argument(
    "--ecm",
    action="store_true",
    help="make an ECM model (default: MDF)",
)
parser.add_argument(
    'network_sbtab',
    type=argparse.FileType('rt'),
    help="Path to input network SBtab"
)
parser.add_argument(
    "outfile",
    type=argparse.FileType('wt'),
    help="Path to output pathway configuration SBtab",
)
args = parser.parse_args()

comp_contrib = ComponentContribution()

pp = Pathway.from_network_sbtab(args.network_sbtab, comp_contrib=comp_contrib)
pp.to_sbtab(args.outfile)
