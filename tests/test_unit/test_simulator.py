import numpy as np
import pytest
from equilibrator_api import Q_

from equilibrator_pathway.cost_function import EnzymeCostFunction
from equilibrator_pathway.simulator import EnzymeCostSimulator


@pytest.fixture(scope="module")
def bistable_ecf() -> EnzymeCostFunction:
    Nc = 3
    Nr = 3

    S = np.zeros((Nc, Nr))
    fluxes = np.zeros((Nr, 1)) * Q_("mM/s")
    kcat = np.zeros((Nr, 1)) * Q_("1/s")
    standard_dg = np.zeros((Nr, 1)) * Q_("kJ/mol")
    KMM = np.ones((Nc, Nr)) * Q_("M")
    A_act = np.zeros((Nc, Nr)) * Q_("M")
    K_act = np.ones((Nc, Nr)) * Q_("M")
    A_inh = np.zeros((Nc, Nr)) * Q_("M")
    K_inh = np.ones((Nc, Nr)) * Q_("M")

    # v0: X0 -> X1
    S[0, 0] = -1.0
    S[1, 0] = 1.0
    fluxes[0, 0] = Q_(2.0, "mM/s")
    kcat[0, 0] = Q_(20.0, "1/s")
    standard_dg[0, 0] = Q_(-30, "kJ/mol")
    KMM[0, 0] = Q_(1e-2, "M")
    KMM[1, 0] = Q_(1e-4, "M")

    # v1: X1 -> X2
    S[1, 1] = -1.0
    S[2, 1] = 1.0
    fluxes[1, 0] = Q_(1.0, "mM/s")
    kcat[1, 0] = Q_(8.0, "1/s")
    standard_dg[1, 0] = Q_(-20, "kJ/mol")
    KMM[1, 1] = Q_(1e-4, "M")
    KMM[2, 1] = Q_(1e-1, "M")

    # v2: X1 -> X2
    S[1, 2] = -1.0
    S[2, 2] = 1.0
    fluxes[2, 0] = Q_(1.0, "mM/s")
    kcat[2, 0] = Q_(0.1, "1/s")
    standard_dg[2, 0] = Q_(-20, "kJ/mol")
    KMM[1, 2] = Q_(1e-3, "M")
    KMM[2, 2] = Q_(1e-1, "M")

    # add a negative allosteric feedback from X1 to reaction 1
    A_inh[1, 1] = Q_(2, "M")
    K_inh[1, 1] = Q_(2e-5, "M")

    # add a positive allosteric feedback from X1 to reaction 2
    A_act[1, 2] = Q_(2, "M")
    K_act[1, 2] = Q_(1e-3, "M")

    ln_conc_lb = np.log(np.array([1e-4, 1e-6, 1e-4]))
    ln_conc_ub = np.log(np.array([1e-4, 1e-2, 1e-4]))

    return EnzymeCostFunction(
        S,
        fluxes=fluxes,
        kcat=kcat,
        standard_dg=standard_dg,
        KMM=KMM,
        ln_conc_lb=ln_conc_lb,
        ln_conc_ub=ln_conc_ub,
        A_act=A_act,
        A_inh=A_inh,
        K_act=K_act,
        K_inh=K_inh,
    )


def test_simulate(bistable_ecf):
    Nc, Nr = bistable_ecf.S.shape
    ln_c = np.log(1e-4) * np.ones((Nc, 1))
    E = np.array([0.5, 0.225, 0.325])  # [g]

    simu = EnzymeCostSimulator(bistable_ecf)

    v_inf, lnC_inf = simu.Simulate(ln_c, E)

    assert v_inf == pytest.approx(0.0912, rel=0.01)
    assert lnC_inf[0] == pytest.approx(np.log(1e-4), rel=0.01)
    assert lnC_inf[1] == pytest.approx(-2.15, rel=0.01)
    assert lnC_inf[2] == pytest.approx(np.log(1e-4), rel=0.01)
