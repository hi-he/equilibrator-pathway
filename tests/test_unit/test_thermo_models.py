# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import matplotlib.pyplot as plt
import numpy as np
import pytest
from equilibrator_api import Q_
from path import Path
from sbtab import SBtab

from equilibrator_pathway import Pathway
from equilibrator_pathway.cost_function import EnzymeCostFunction
from equilibrator_pathway.ecm_model import ECMmodel
from equilibrator_pathway.util import RT


@pytest.fixture(scope="module")
def test_dir() -> Path:
    return Path(__file__).abspath().parent


@pytest.mark.parametrize(
    "sbtab_fname",
    [("ccm_without_dgs.tsv"), ("ccm_with_dgs.tsv"), ("ccm_with_keq.tsv")],
)
def test_pathway(sbtab_fname, test_dir, comp_contribution_legacy):
    sbtabdoc = SBtab.read_csv(str(test_dir / sbtab_fname), "model")

    pathway = Pathway.from_sbtab(
        sbtabdoc, comp_contrib=comp_contribution_legacy
    )
    mdf_data = pathway.calc_mdf()
    assert mdf_data.mdf.m_as("kJ/mol") == pytest.approx(2.065, rel=0.01)

    sp = mdf_data.reaction_df.set_index("reaction_id").shadow_price
    assert sp["MDH_R00342"] == pytest.approx(1.0, abs=1e-5)


@pytest.mark.parametrize(
    "sbtab_fname",
    [("ccm_without_dgs.tsv"), ("ccm_with_dgs.tsv"), ("ccm_with_keq.tsv")],
)
def test_sbtab_thermodynamics(sbtab_fname, test_dir, comp_contribution_legacy):
    sbtabdoc = SBtab.read_csv(str(test_dir / sbtab_fname), "model")

    model = ECMmodel.from_sbtab(sbtabdoc, comp_contrib=comp_contribution_legacy)
    assert model.ecf.standard_dg_over_rt[0] == pytest.approx(-17.8, rel=0.01)

    mdf, _ = model.MDF()
    assert mdf.m_as("kJ/mol") == pytest.approx(2.065, rel=0.01)


def test_sbtab_mdmc(test_dir, comp_contribution_legacy):
    sbtabdoc = SBtab.read_csv(str(test_dir / "ccm_with_dgs.tsv"), "model")

    pathway = Pathway.from_sbtab(
        sbtabdoc, comp_contrib=comp_contribution_legacy
    )
    mdmc_data = pathway.calc_mdmc()

    assert mdmc_data.objective_values.iat[0] == pytest.approx(11.93, rel=0.01)
    assert mdmc_data.objective_values.iat[-1] == pytest.approx(204.26, rel=0.01)

    assert mdmc_data.zscore_df.loc[
        "3_Phospho_D_glyceroyl_phosphate", 0.0
    ] == pytest.approx(-1.475, rel=0.01)
    assert mdmc_data.zscore_df.loc["Isocitrate", 10.0] == pytest.approx(
        -3.120, rel=0.01
    )
