# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import matplotlib.pyplot as plt
import numpy as np
import pytest
from equilibrator_api import Q_
from path import Path
from sbtab import SBtab

from equilibrator_pathway.cost_function import EnzymeCostFunction
from equilibrator_pathway.ecm_model import ECMmodel
from equilibrator_pathway.util import RT


@pytest.fixture(scope="module")
def test_dir() -> Path:
    return Path(__file__).abspath().parent


@pytest.fixture(scope="module")
def toy_ecf() -> EnzymeCostFunction:
    Nr = 3
    Nc = 4
    S = np.zeros((Nc, Nr))

    S[0, 0] = -1
    S[1, 0] = 1
    S[2, 0] = 1
    S[1, 1] = -1
    S[2, 1] = 1
    S[2, 2] = -1
    S[3, 2] = 1

    fluxes = np.array([1.0, 1.0, 2.0], ndmin=2).T * Q_(1, "dimensionless")
    kcat = np.array([1.0, 1.0, 1.0], ndmin=2).T * Q_(1, "1/s")
    dGm_r = np.array([-3.0, -2.0, -3.0], ndmin=2).T * Q_(1, "kJ/mol")
    dG0_r = dGm_r - S.T @ np.ones((Nc, 1)) * RT * np.log(1e-3)

    KMM = np.ones(S.shape) * Q_("M")
    KMM[S < 0] = Q_(90, "mM")
    KMM[S > 0] = Q_(10, "mM")

    ln_conc_lb = np.ones(Nc) * np.log(1e-9)
    ln_conc_ub = np.ones(Nc) * np.log(1e-1)

    A_act = np.zeros(S.shape) * Q_("M")
    A_inh = np.zeros(S.shape) * Q_("M")
    K_act = np.ones(S.shape) * Q_("M")
    K_inh = np.ones(S.shape) * Q_("M")

    return EnzymeCostFunction(
        S,
        fluxes,
        kcat,
        dG0_r,
        KMM,
        ln_conc_lb=ln_conc_lb,
        ln_conc_ub=ln_conc_ub,
        mw_enz=None,
        mw_met=None,
        A_act=A_act,
        A_inh=A_inh,
        K_act=K_act,
        K_inh=K_inh,
    )


def test_toy_ecm(toy_ecf):
    np.random.seed(2013)
    conc0 = [1e-1, 1e-2, 1e-3, 1e-8]
    ln_conc0 = np.log(np.array([conc0]).T)
    ecm_score, lnC = toy_ecf.ECM(ln_conc0)
    concs = np.exp(lnC)
    costs = toy_ecf.ECF(lnC)

    assert ecm_score == pytest.approx(12.34, rel=0.1)
    assert concs[0, 0] == pytest.approx(0.1, rel=0.1)
    assert concs[1, 0] == pytest.approx(0.015, rel=0.1)
    assert concs[2, 0] == pytest.approx(0.0067, rel=0.1)
    assert concs[3, 0] == pytest.approx(1e-8, rel=0.1)
    assert costs[0, 0] == pytest.approx(3.93, rel=0.1)
    assert costs[1, 0] == pytest.approx(3.08, rel=0.1)
    assert costs[2, 0] == pytest.approx(5.22, rel=0.1)


def test_sbtab(test_dir, comp_contribution_legacy):
    np.random.seed(2013)
    modeldata_fname = str(test_dir / "ccm_with_dgs.tsv")
    sbtabdoc = SBtab.read_csv(modeldata_fname, "model")
    model = ECMmodel.from_sbtab(sbtabdoc, comp_contrib=comp_contribution_legacy)

    # Test correct Parameter loading
    idx_r = model.reaction_ids.index("PTS_RPTSsy")
    idx_c = model.compound_ids.index("Pyruvate")
    assert model.ecf.kcat[idx_r] == pytest.approx(38.7, rel=0.01)
    assert model.ecf.KMM[idx_c, idx_r] == pytest.approx(0.20e-3, rel=0.01)

    idx_r = model.reaction_ids.index("GAP_R01061")
    idx_c = model.compound_ids.index("NADplus")
    assert model.ecf.kcat[idx_r] == pytest.approx(70.74, rel=0.01)
    assert model.ecf.KMM[idx_c, idx_r] == pytest.approx(0.276e-3, rel=0.01)

    # Test MDF

    mdf_score, lnC_MDF = model.MDF()
    assert mdf_score.m_as("kJ/mol") == pytest.approx(2.065, rel=0.01)

    cid2lnC_MDF = dict(zip(model.compound_ids, lnC_MDF.flat))
    # assert cid2lnC_MDF["Glycerone_phosphate"] == pytest.approx(-5.475, rel=0.01)
    assert cid2lnC_MDF["Acetyl_CoA"] == pytest.approx(-4.61, rel=0.01)
    # assert cid2lnC_MDF["D_Glucose"] == pytest.approx(-4.42, rel=0.01)

    score_MDF = model.ecf.ECF(lnC_MDF)
    rid2score_MDF = dict(zip(model.reaction_ids, score_MDF.flat))
    assert rid2score_MDF["ACN_R01900"] == pytest.approx(1.05e-4, rel=0.01)
    assert rid2score_MDF["PGH_R00658"] == pytest.approx(1.42e-4, rel=0.01)
    assert rid2score_MDF["GAP_R01061"] == pytest.approx(1.66e-4, rel=0.01)
    assert score_MDF.sum() == pytest.approx(9.6e-4, rel=0.1)

    # Test ECM

    np.random.seed(99)
    ecm_score, lnC_ECM = model.ECM(ln_conc0=lnC_MDF)
    cid2lnC_ECM = dict(zip(model.compound_ids, lnC_ECM.round(2).flat))
    enzyme_costs = model.ecf.ECF(lnC_ECM)
    rid2score_ECM = dict(zip(model.reaction_ids, enzyme_costs.flat))

    assert ecm_score == pytest.approx(61.48, rel=0.01)
    assert cid2lnC_ECM["Glycerone_phosphate"] == pytest.approx(-6.02, rel=0.01)
    assert cid2lnC_ECM["Acetyl_CoA"] == pytest.approx(-6.47, rel=0.01)
    assert cid2lnC_ECM["D_Glucose"] == pytest.approx(-4.42, rel=0.01)

    assert rid2score_ECM["ACN_R01900"] == pytest.approx(7.35e-5, rel=0.01)
    assert rid2score_ECM["PGH_R00658"] == pytest.approx(5.22e-5, rel=0.01)
    assert rid2score_ECM["GAP_R01061"] == pytest.approx(1.37e-4, rel=0.01)
    assert enzyme_costs.sum() == pytest.approx(6.75e-4, rel=0.1)


def test_validation_sbtab(test_dir, comp_contribution_legacy):
    modeldata_fname = str(test_dir / "ecoli_noor_2016_ecm.tsv")
    validdata_fname = str(test_dir / "ecoli_noor_2016_reference.tsv")

    sbtabdoc_pathway = SBtab.read_csv(modeldata_fname, "model")
    sbtabdoc_validate = SBtab.read_csv(validdata_fname, "model")

    model = ECMmodel.from_sbtab(
        sbtabdoc_pathway, comp_contrib=comp_contribution_legacy
    )
    model.AddValidationData(sbtabdoc_validate)

    np.random.seed(1982)

    _, lnC_MDF = model.MDF()
    _, lnC_ECM = model.ECM()
    _ = model.to_sbtab(lnC_ECM)

    fig = plt.figure(figsize=(14, 5))
    ax_MDF = fig.add_subplot(1, 2, 1)
    model.PlotEnzymeDemandBreakdown(lnC_MDF, ax_MDF, plot_measured=True)
    ax_ECM = fig.add_subplot(1, 2, 2, sharey=ax_MDF)
    model.PlotEnzymeDemandBreakdown(lnC_ECM, ax_ECM, plot_measured=True)
    plt.close(fig)

    fig = plt.figure(figsize=(16, 5))
    ax = fig.add_subplot(1, 3, 1, xscale="log", yscale="log")
    ax.set_title("Metabolite Concentrations")
    model.ValidateMetaboliteConcentrations(lnC_ECM, ax)
    ax = fig.add_subplot(1, 3, 2, xscale="log", yscale="log")
    ax.set_title("Enzyme Concentrations")
    model.ValidateEnzymeConcentrations(lnC_ECM, ax)
    ax = fig.add_subplot(1, 3, 3)
    vols, labels, colors = model._GetVolumeDataForPlotting(lnC_ECM)
    ax.pie(vols, labels=labels, colors=colors)
    ax.set_title("total weight = %.2g [g/L]" % sum(vols))
    plt.close(fig)
