"""thermo_models contains tools for running MDF and displaying results."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from equilibrator_api import Q_, standard_concentration, ureg
from matplotlib.collections import LineCollection
from matplotlib.lines import Line2D

from .util import RT


class PathwayMdfSolution(object):
    """Handle MDF results.

    PathwayMDFData is a container class for MDF results, with plotting
    capabilities.
    """

    def __init__(
        self,
        thermo_model: "PathwayThermoModel",
        B: float,
        lnC: np.array,
        y: np.array,
        reaction_prices: np.array,
        compound_prices: np.array,
    ) -> object:
        """Create PathwayMDFData object.

        :param pathway: a Pathway object
        :param B: the MDF value (in units of RT)
        :param I_dir: matrix of flux directions
        :param lnC: log concentrations at MDF optimum
        :param y: uncertainty matrix coefficients at MDF optimum
        :param reaction_prices: shadow prices of reactions
        :param compound_prices: shadow prices of compound concentrations
        """
        self.mdf = Q_(B, "kJ/mol")
        self.lnC_primal = lnC
        self.lnC_mu = thermo_model.lnC_mu
        self.lnC_sigma = thermo_model.lnC_sigma
        self.confidence_interval = thermo_model.CONFIDENCE_INTERVAL

        concentrations = np.exp(lnC) * standard_concentration

        standard_dg_primes = thermo_model.standard_dg_primes

        physiological_dg_primes = (
            standard_dg_primes
            + PathwayMdfSolution.phys_dg_correction(thermo_model.pathway)
        )

        optimized_dg_primes = (
            standard_dg_primes
            + PathwayMdfSolution.dg_correction(
                thermo_model.pathway, concentrations
            )
        )

        # add the calculated error values (due to the ΔG'0 uncertainty)
        if thermo_model.dg_sigma is not None:
            optimized_dg_primes += thermo_model.dg_sigma @ y.squeeze()

        # adjust ΔG to flux directions and convert to kJ/mol
        standard_dg_primes = thermo_model.I_dir @ standard_dg_primes
        physiological_dg_primes = thermo_model.I_dir @ physiological_dg_primes
        optimized_dg_primes = thermo_model.I_dir @ optimized_dg_primes

        # all dG values are in units of RT, so we convert them to kJ/mol
        reaction_data = zip(
            thermo_model.pathway.reaction_ids,
            thermo_model.pathway.reaction_formulas,
            thermo_model.pathway.fluxes,
            standard_dg_primes,
            physiological_dg_primes,
            optimized_dg_primes,
            reaction_prices,
        )
        self.reaction_df = pd.DataFrame(
            data=list(reaction_data),
            columns=[
                "reaction_id",
                "reaction_formula",
                "flux",
                "standard_dg_prime",
                "physiological_dg_prime",
                "optimized_dg_prime",
                "shadow_price",
            ],
        )

        compound_data = zip(
            thermo_model.pathway.compound_names, concentrations, compound_prices
        )

        self.compound_df = pd.DataFrame(
            data=list(compound_data),
            columns=["compound", "concentration", "shadow_price"],
        )
        lbs, ubs = thermo_model.pathway.bounds
        self.compound_df["lower_bound"] = list(map(lambda x: x.m_as("M"), lbs))
        self.compound_df["upper_bound"] = list(map(lambda x: x.m_as("M"), ubs))

    @staticmethod
    def phys_dg_correction(pathway: object) -> Q_:
        """Add the effect of reactant physiological concentrations on the ΔG'.

        :param pathway: Pathway object
        :return: the reaction energies
        """
        dg_adj = np.array(
            [
                r.physiological_dg_correction().m_as("")
                for r in pathway.reactions
            ]
        )
        return dg_adj * RT

    @staticmethod
    @ureg.check(None, "[concentration]")
    def dg_correction(pathway: object, concentrations: np.ndarray) -> Q_:
        """Add the effect of reactant concentrations on the ΔG'.

        :param pathway: Pathway object
        :param concentrations: a NumPy array of concentrations
        :return: the reaction energies
        """
        log_conc = np.log(concentrations / standard_concentration).squeeze()

        if np.isnan(pathway.standard_dg_primes).any():
            dg_adj = np.zeros(pathway.S.shape[1])
            for r in range(pathway.S.shape[1]):
                reactants = list(pathway.S[:, r].nonzero()[0].flat)
                dg_adj[r] = log_conc[reactants] @ pathway.S[reactants, r]
        else:
            dg_adj = pathway.S.T.values @ log_conc

        return dg_adj * RT

    def plot_concentrations(self, ax: plt.Axes) -> None:
        """Plot compound concentrations.

        :return: matplotlib Figure
        """
        ureg.setup_matplotlib(True)

        data_df = self.compound_df.copy()
        data_df["y"] = np.arange(0, data_df.shape[0])
        data_df["shadow_sign"] = data_df.shadow_price.apply(np.sign)

        for shadow_sign, group_df in data_df.groupby("shadow_sign"):
            if shadow_sign == -1:
                color, marker = ("blue", ">")
            elif shadow_sign == 1:
                color, marker = ("red", "<")
            else:
                color, marker = ("grey", "d")

            group_df.plot.scatter(
                x="concentration",
                y="y",
                s=40,
                c=color,
                marker=marker,
                ax=ax,
                zorder=2,
                colorbar=False,
            )
        ax.set_ylabel("")
        ax.set_yticks(data_df.y)

        for j, row in enumerate(data_df.itertuples(index=True)):
            met = row.Index
            ax.plot(
                [row.lower_bound, row.upper_bound],
                [row.y, row.y],
                color="lightgrey",
                linewidth=3,
                zorder=1,
            )

        ax.set_yticklabels(data_df["compound"], fontsize=9)
        ax.set_xlabel("Concentration (M)")
        ax.set_xscale("log")
        ax.set_ylim(-0.5, data_df.shape[0] + 0.5)

    def plot_driving_forces(self, ax: plt.Axes) -> None:
        """Plot cumulative delta-G profiles.

        :return: matplotlib Figure
        """
        ureg.setup_matplotlib(True)

        data_df = self.reaction_df.copy()
        data_df.reindex()

        data_df[
            "cml_dgm"
        ] = self.reaction_df.physiological_dg_prime.cumsum().apply(
            lambda x: x.m_as("kJ/mol")
        )
        data_df[
            "cml_dg_opt"
        ] = self.reaction_df.optimized_dg_prime.cumsum().apply(
            lambda x: x.m_as("kJ/mol")
        )

        xticks = 0.5 + np.arange(data_df.shape[0])
        xticklabels = data_df.reaction_id.tolist()

        yvalues_phy = [0.0] + data_df.cml_dgm.tolist()
        yvalues_opt = [0.0] + data_df.cml_dg_opt.tolist()

        ax.plot(
            yvalues_phy,
            label="Physiological concentrations (1 mM)",
            color="lightgreen",
            linestyle="--",
            zorder=1,
        )
        ax.plot(
            yvalues_opt,
            label="MDF-optimized concentrations",
            color="grey",
            linewidth=2,
            zorder=2,
        )
        lines = [
            ((i, yvalues_opt[i]), (i + 1, yvalues_opt[i + 1]))
            for i in data_df[data_df.shadow_price != 0].index
        ]
        lines = LineCollection(
            lines,
            label="Bottleneck reactions",
            linewidth=3,
            color="red",
            linestyle="-",
            zorder=3,
            alpha=1,
        )
        ax.add_collection(lines)
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels, rotation=45, ha="center")
        ax.set_xlim(0, data_df.shape[0])

        ax.set_xlabel("Reaction Step")
        ax.set_ylabel(r"Cumulative $\Delta_r G^\prime$ (kJ/mol)")
        ax.legend(loc="best")
        ax.set_title(f"MDF = {self.mdf:.2f}")
