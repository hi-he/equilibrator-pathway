"""analyze pathways using thermodynamic models."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from types import ModuleType
from typing import Dict, List, Optional, Union

import numpy as np
import pandas as pd
from equilibrator_api import Q_, ComponentContribution, Reaction
from sbtab import SBtab

from .bounds import Bounds
from .mdf_solution import PathwayMdfSolution
from .mdmc_solution import PathwayMdmcSolution
from .model import StoichiometricModel
from .thermo_models import PathwayThermoModel


class Pathway(StoichiometricModel):
    """A pathway parsed from user input.

    Designed for checking input prior to converting to a stoichiometric model.
    """

    def __init__(
        self,
        reactions: List[Reaction],
        fluxes: Q_,
        comp_contrib: Optional[ComponentContribution] = None,
        standard_dg_primes: Optional[np.ndarray] = None,
        dg_covariance: Optional[np.ndarray] = None,
        bounds: Optional[Bounds] = None,
        config_dict: Optional[Dict[str, str]] = None,
    ) -> None:
        """Initialize a Pathway object.

        Parameters
        ----------
        reactions : List[Reaction]
            a list of Reaction objects
        fluxes : Quantity
            relative fluxes in same order as
        comp_contrib : ComponentContribution
            a ComponentContribution object
        standard_dg_primes : ndarray, optional
            reaction energies (in kJ/mol)
        dg_covariance : ndarray, optional
            square root of the uncertainty covariance matrix
            (in kJ^2/mol^2)
        bounds : Bounds, optional
            bounds on metabolite concentrations (by default uses the
            "data/cofactors.csv" file in `equilibrator-api`)
        config_dict : dict, optional
            configuration parameters for Pathway analysis
        """
        super(Pathway, self).__init__(
            reactions=reactions,
            comp_contrib=comp_contrib,
            standard_dg_primes=standard_dg_primes,
            dg_covariance=dg_covariance,
            bounds=bounds,
            config_dict=config_dict,
        )

        Nr = len(reactions)
        assert fluxes.unitless or fluxes.check("[concentration]/[time]")
        self.fluxes = fluxes.flatten()
        assert self.fluxes.shape == (Nr,)

    @property
    def net_reaction(self) -> Reaction:
        """Calculate the sum of all the reactions in the pathway.

        :return: the net reaction
        """
        net_rxn_stoich = self.S @ self.fluxes.magnitude
        net_rxn_stoich = net_rxn_stoich[net_rxn_stoich != 0]
        sparse = net_rxn_stoich.to_dict()
        return Reaction(sparse)

    def _make_pathway_thermo_model(
        self, optlang_interface: Optional[ModuleType] = None
    ) -> PathwayThermoModel:
        """Create a PathwayThermoModel for this Pathway.

        Returns
        -------
        a PathwayThermoModel object
        """
        return PathwayThermoModel(self, optlang_interface=optlang_interface)

    def calc_mdf(
        self, optlang_interface: Optional[ModuleType] = None
    ) -> PathwayMdfSolution:
        """Calculate the Max-min Driving Force.

        Returns
        -------
        a PathwayMdfSolution object with the results
        """
        return self._make_pathway_thermo_model(
            optlang_interface=optlang_interface
        ).find_mdf()

    def calc_mdmc(
        self, optlang_interface: Optional[ModuleType] = None
    ) -> PathwayMdmcSolution:
        """Calculate the Max-min Driving Force.

        Returns
        -------
        a PathwayMdfSolution object with the results
        """
        return self._make_pathway_thermo_model(
            optlang_interface=optlang_interface
        ).find_mdmc()

    @classmethod
    def from_network_sbtab(
        cls,
        filename: Union[str, SBtab.SBtabDocument],
        comp_contrib: Optional[ComponentContribution] = None,
        freetext: bool = True,
        bounds: Optional[Bounds] = None,
    ) -> object:
        """Initialize a Pathway object using a 'network'-only SBtab.

        Parameters
        ----------
        filename : str, SBtabDocument
            a filename containing an SBtabDocument (or the SBtabDocument
            object itself) defining the network (topology) only
        comp_contrib : ComponentContribution, optional
            a ComponentContribution object needed for parsing and searching
            the reactions. also used to set the aqueous parameters (pH, I, etc.)
        freetext : bool, optional
            a flag indicating whether the reactions are given as free-text (i.e.
            common names for compounds) or by standard database accessions
            (Default value: `True`)
        bounds : Bounds, optional
            bounds on metabolite concentrations (by default uses the
            "data/cofactors.csv" file in `equilibrator-api`)

        Returns
        -------
            a Pathway object
        """
        (
            reaction_df,
            comp_contrib,
            config_dict,
        ) = StoichiometricModel._read_network_sbtab(
            filename, comp_contrib, freetext
        )
        fluxes = reaction_df.RelativeFlux.apply(float).values * Q_(
            "dimensionless"
        )
        pp = Pathway(
            reactions=reaction_df.Reaction.tolist(),
            fluxes=fluxes,
            comp_contrib=comp_contrib,
            bounds=bounds,
            config_dict=config_dict,
        )
        # TODO: replace this with Compound.get_common_name()
        pp.set_compound_names(comp_contrib.ccache.get_compound_first_name)
        return pp

    @classmethod
    def from_sbtab(
        cls,
        filename: Union[str, SBtab.SBtabDocument],
        comp_contrib: Optional[ComponentContribution] = None,
    ) -> "Pathway":
        """Parse and SBtabDocument and return a StoichiometricModel.

        Parameters
        ----------
        filename : str or SBtabDocument
            a filename containing an SBtabDocument (or the SBtabDocument
            object itself) defining the pathway
        comp_contrib : ComponentContribution, optional
            a ComponentContribution object needed for parsing and searching
            the reactions. also used to set the aqueous parameters (pH, I, etc.)

        Returns
        -------
        pathway: Pathway
            A Pathway object based on the configuration SBtab

        """
        comp_contrib = comp_contrib or ComponentContribution()

        (
            sbtabdoc,
            reactions,
            reaction_ids,
            standard_dg_primes,
            dg_covariance,
            bounds,
            config_dict,
            name_to_compound,
        ) = cls._read_model_sbtab(filename, comp_contrib)

        # Read the Flux table
        # ---------------------------
        flux_sbtab = sbtabdoc.get_sbtab_by_id("Flux")
        assert flux_sbtab is not None, "Cannot find a 'Flux' table in the SBtab"
        flux_df = flux_sbtab.to_data_frame()
        assert set(flux_df.Reaction.unique()) == set(reaction_ids), (
            r"'Reaction' and 'Flux' tables are not 100% compatible (check"
            r"that the list of IDs is the same"
        )

        fluxes = (
            flux_df.set_index("Reaction")
            .loc[reaction_ids, "Value"]
            .apply(float)
        )

        fluxes = np.array(fluxes.values, ndmin=2, dtype=float).T

        try:
            # convert fluxes to M/s if they are in some other absolute unit
            flux_unit = flux_sbtab.get_attribute("Unit")
            fluxes *= Q_(1.0, flux_unit)
        except SBtab.SBtabError:
            # otherwise, assume these are relative fluxes
            fluxes *= Q_("dimensionless")

        pp = Pathway(
            reactions=reactions,
            fluxes=fluxes,
            comp_contrib=comp_contrib,
            standard_dg_primes=standard_dg_primes,
            dg_covariance=None,
            bounds=bounds,
            config_dict=config_dict,
        )
        # override the compound names with the ones in the SBtab
        compound_to_name = dict(map(reversed, name_to_compound.items()))
        pp.set_compound_names(compound_to_name.get)
        return pp

    def to_sbtab(self) -> SBtab.SBtabDocument:
        """Export the pathway to an SBtabDocument."""
        sbtabdoc = super(Pathway, self).to_sbtab()

        # add the flux table
        flux_df = pd.DataFrame(
            data=[
                ("rate of reaction", rxn.rid, f)
                for rxn, f in zip(self.reactions, self.fluxes.magnitude)
            ],
            columns=["QuantityType", "Reaction", "Value"],
        )
        flux_sbtab = SBtab.SBtabTable.from_data_frame(
            df=flux_df, table_id="Flux", table_type="Quantity"
        )
        flux_sbtab.change_attribute("Unit", self.fluxes.units)
        sbtabdoc.add_sbtab(flux_sbtab)

        return sbtabdoc
