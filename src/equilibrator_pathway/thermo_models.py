"""thermo_models contains tools for running MDF and displaying results."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import importlib
import logging
from types import ModuleType
from typing import Optional

import numpy as np
import optlang
import pandas as pd
from equilibrator_api import Q_
from scipy.linalg import fractional_matrix_power

from .mdf_solution import PathwayMdfSolution
from .mdmc_solution import PathwayMdmcSolution
from .util import RT


class PathwayThermoModel(object):
    """Container for doing pathway-level thermodynamic analysis."""

    CONFIDENCE_INTERVAL = 1.96
    MINIMAL_STDEV = 1e-3

    def __init__(
        self, pathway, optlang_interface: Optional[ModuleType] = None
    ) -> object:
        """Create a model for running MDF analysis.

        Arguments
        ---------
        pathway : Pathway
            a Pathway object defining the reactions and bounds
    
        optlang_interface : ModuleType
            override the choice of which optlang solver to use (default: None)
        """
        self.pathway = pathway
        # we keep the index to H2O handy, since we often need to remove
        # water from the list of compounds
        self.idx_water = self.pathway.S.index.tolist().index(pathway.water)

        self.Nc, self.Nr = pathway.S.shape
        self.optlang_interface = optlang_interface

        # Make sure ΔG0_r' is the right size
        assert pathway.standard_dg_primes.shape == (
            self.Nr,
        ), "standard dG required for all reactions"

        self.standard_dg_primes = pathway.standard_dg_primes

        if pathway.dg_covariance is not None:
            assert pathway.dg_covariance.shape == (
                self.Nr,
                self.Nr,
            ), "uncertainty in dG required for all reactions"

            self.dg_sigma = fractional_matrix_power(
                pathway.dg_covariance.m_as("kJ**2/mol**2"), 0.5
            ) * Q_("kJ/mol")
        else:
            self.dg_sigma = None

        self.I_dir = np.diag(np.sign(self.pathway.fluxes.magnitude).flat)

        # In MDMC we use Z-score to describe distribution within the metabolite
        # so we need to convert the "hard" bounds into a Gaussian distribution
        # we assume that the given bounds represent the 95% confidence interval
        # a the Gaussian distribution (i.e. [mu - 1.96*sigma, mu + 1.96*sigma])
        # Therefore:
        #              mu = (ub + lb) / 2
        #           sigma = (ub - lb) / 3.92
        self.lnC_mu = (self.pathway.ln_conc_ub + self.pathway.ln_conc_lb) / 2.0
        self.lnC_sigma = (self.pathway.ln_conc_ub - self.pathway.ln_conc_lb) / (
            self.CONFIDENCE_INTERVAL * 2.0
        )

    def _get_optlang_interface(self, quadratic=False):
        if self.optlang_interface:
            return self.optlang_interface
        if quadratic:
            priorities = ["gurobi", "cplex", "osqp"]
        else:
            priorities = ["gurobi", "cplex", "glpk", "scipy"]
        for solver_name in priorities:
            if optlang.available_solvers.get(solver_name.upper(), False):
                return importlib.import_module(
                    f"optlang.{solver_name}_interface", "optlang"
                )
        if quadratic:
            raise ImportError("Cannot find an QP solver for optlang")
        else:
            raise ImportError("Cannot find an LP solver for optlang")

    def _make_max_min_driving_force_lp(
        self, optlang_interface: ModuleType
    ) -> optlang.Model:
        """Create primal LP problem for Min-max Thermodynamic Driving Force.

        Returns
        -------
        the linear problem object, and the three types of variables as arrays.
        """
        lp = optlang_interface.Model(name="MDF")

        # ln-concentration variables (where the units are in M before taking
        # the log)
        lnC = [
            optlang_interface.Variable(f"var:log_conc:{j}")
            for j in range(self.Nc)
        ]

        # the margin variable representing the MDF in units of kJ/mol
        B = optlang_interface.Variable("var:minimal_driving_force:0")

        if self.dg_sigma is not None:
            # define the ΔG'0 covariance eigenvariables
            y = [
                optlang_interface.Variable(
                    f"var:covariance_eigenvalue:{j}",
                    lb=-self.pathway.stdev_factor,
                    ub=self.pathway.stdev_factor,
                )
                for j in range(self.Nr)
            ]
        else:
            y = []

        for j in range(self.Nr):
            direction = self.I_dir[j, j]
            if direction == 0:  # an inactive reaction does not constrain ΔGs
                continue

            row = [
                RT.m_as("kJ/mol") * self.pathway.S.iloc[i, j] * lnC[i]
                for i in range(self.Nc)
            ]

            if self.dg_sigma is not None:
                # add the uncertainty value based on the covariance
                # eigenvariables (y)
                row += [
                    self.dg_sigma[j, i].m_as("kJ/mol") * y[i]
                    for i in range(self.Nr)
                ]
            if direction == 1:
                lp.add(
                    optlang_interface.Constraint(
                        sum(row) + B,
                        ub=-self.standard_dg_primes[j].m_as("kJ/mol"),
                        name=f"cnstr:driving_force:{j}",
                    )
                )
            else:
                lp.add(
                    optlang_interface.Constraint(
                        sum(row) - B,
                        lb=-self.standard_dg_primes[j].m_as("kJ/mol"),
                        name=f"cnstr:driving_force:{j}",
                    )
                )

        return lp

    def _force_concentration_bounds(
        self,
        optlang_interface: ModuleType,
        lp: optlang.Model,
        constant_only: bool = False,
    ) -> None:
        """Add lower and upper bounds for the log concentrations.
        
        Arguments
        ---------
        lp : optlang.Model
        
        constant_only : bool (optional)
            Whether to only constrain the compounds with a narrow range
            (i.e. ones with a constant concentration) or all compounds.
        """
        for j in range(self.Nc):
            if constant_only and self.lnC_sigma[j] > self.MINIMAL_STDEV:
                continue

            lnC = lp.variables.__getattr__(f"var:log_conc:{j}")
            lb = self.lnC_mu[j] - self.CONFIDENCE_INTERVAL * self.lnC_sigma[j]
            ub = self.lnC_mu[j] + self.CONFIDENCE_INTERVAL * self.lnC_sigma[j]
            lp.add(
                optlang_interface.Constraint(
                    lnC, lb=lb, ub=ub, name=f"cnstr:log_conc:{j}"
                )
            )

    def find_mdf(self) -> PathwayMdfSolution:
        """Find the MDF (Max-min Driving Force).

        Returns
        -------
        a PathwayMDFData object with the results of MDF analysis.
        """
        optlang_interface = self._get_optlang_interface(quadratic=False)
        lp_mdf = self._make_max_min_driving_force_lp(optlang_interface)
        self._force_concentration_bounds(
            optlang_interface, lp_mdf, constant_only=False
        )

        B = lp_mdf.variables.__getattr__("var:minimal_driving_force:0")
        lp_mdf.objective = optlang_interface.Objective(B, direction="max")

        if lp_mdf.optimize() != "optimal":
            logging.warning("LP status %s", lp_mdf.status)
            raise Exception("Cannot solve MDF optimization problem")

        # the MDF solution
        primal_B = B.primal

        # covariance eigenvalue prefactors
        primal_y = np.array(
            [
                lp_mdf.primal_values.get(f"var:covariance_eigenvalue:{j}", 0.0)
                for j in range(self.Nr)
            ],
            ndmin=2,
        ).T

        # log concentrations (excluding H2O)

        primal_lnC = np.array(
            [lp_mdf.primal_values[f"var:log_conc:{j}"] for j in range(self.Nc)],
            ndmin=2,
        ).T

        compound_prices = np.array(
            [
                lp_mdf.shadow_prices[f"cnstr:log_conc:{j}"]
                for j in range(self.Nc)
            ],
            ndmin=2,
        ).T.round(5)

        reaction_prices = np.array(
            [
                np.abs(
                    lp_mdf.shadow_prices.get(f"cnstr:driving_force:{j}", 0.0)
                )
                for j in range(self.Nr)
            ],
            ndmin=2,
        ).T.round(5)

        return PathwayMdfSolution(
            self,
            primal_B,
            primal_lnC,
            primal_y,
            reaction_prices,
            compound_prices,
        )

    def _add_zscore_objective(
        self, optlang_interface: ModuleType, lp: optlang.Model
    ):
        """Set the Z-score as the new objective."""

        lnC = [
            lp.variables.__getattr__(f"var:log_conc:{j}")
            for j in range(self.Nc)
        ]

        zscore_sum = [
            (lnC[j] - self.lnC_mu[j]) ** 2 / self.lnC_sigma[j] ** 2
            for j in range(self.Nc)
            if self.lnC_sigma[j] > self.MINIMAL_STDEV
        ]
        lp.objective = optlang_interface.Objective(
            sum(zscore_sum), direction="min"
        )

    def find_mdmc(
        self,
        min_lb: float = 0.0,
        max_lb: Optional[float] = 10.0,
        n_steps: int = 100,
    ) -> PathwayMdmcSolution:
        """Find the MDMC (Maximum Driving-force and Metabolic Consistency.
        
        :return: a PathwayMdmcSolution object with the results of MDMC analysis.
        """
        optlang_interface = self._get_optlang_interface(quadratic=True)
        lp_mdmc = self._make_max_min_driving_force_lp(optlang_interface)
        self._force_concentration_bounds(
            optlang_interface, lp_mdmc, constant_only=True
        )
        self._add_zscore_objective(optlang_interface, lp_mdmc)

        # scan through a range of DF lower bounds to find all possible Pareto
        # optimal solutions to the bi-optimization problem (MDF and Z-score)
        B = lp_mdmc.variables.__getattr__("var:minimal_driving_force:0")
        df_constraint = optlang_interface.Constraint(
            B, lb=0, name="cnstr:minimal_driving_force:0"
        )
        lp_mdmc.add(df_constraint)

        data = []
        for lb in np.linspace(min_lb, max_lb, n_steps):
            df_constraint.lb = lb
            lp_mdmc.optimize()
            if lp_mdmc.status != "optimal":
                raise Exception("Error: LP is not optimal")
            data.append(
                (lb, "primal", "obj", "mdmc", 0, lp_mdmc.objective.value)
            )
            for value_type, value_dict in [
                ("primal", lp_mdmc.primal_values),
                ("shadow_price", lp_mdmc.shadow_prices),
                ("reduced_cost", lp_mdmc.reduced_costs),
            ]:
                for k, v in value_dict.items():
                    var_type, name, idx = k.split(":")
                    idx = int(idx)
                    data.append((lb, value_type, var_type, name, idx, v))

            # calculate the individual z-scores for log_conc variables
            for j in range(self.Nc):
                if self.lnC_sigma[j] > self.MINIMAL_STDEV:
                    lnC = lp_mdmc.variables.__getattr__(f"var:log_conc:{j}")
                    zscore = (lnC.primal - self.lnC_mu[j]) / self.lnC_sigma[j]
                else:
                    zscore = 0
                data.append((lb, "zscore", "var", "log_conc", j, zscore))

        solution_df = pd.DataFrame(
            data=data,
            columns=[
                "df_lb",
                "value_type",
                "var_type",
                "var_name",
                "index",
                "value",
            ],
        )
        return PathwayMdmcSolution(self, solution_df)

        # TODO: write a unit test for MDMC (right now we don't have an
        #  open-source solver that can run on GitLab CI).
